import Mock from 'mockjs'
//FishNovel 顶部导航
const top = [{
        label: "APP运营",
        path: "/featured",
        icon: 'el-icon-document',
        meta: {
            i18n: 'FishNove landroid (light novel)',
        },
        parentId: 0
    },
    // {
    //     label: "广告运营",
    //     path: "/ad/ad_id_config",
    //     icon: 'el-icon-document',
    //     meta: {
    //         i18n: 'FishNove free fiction android',
    //     },
    //     parentId: 4
    // },
    {
        label: "版本推广管理",
        path: "/orangecatversionpromotion/channelmanager",
        icon: 'el-icon-document',
        meta: {
            i18n: 'FishNovel free novel android',
        },
        parentId: 2
    },
    {
        label: "审核",
        path: "/orangefinancialaudit/moneywithdraw",
        icon: 'el-icon-document',
        meta: {
            i18n: 'FishNovel free novel android',
        },
        parentId: 1
    },
    {
        label: "数据",
        path: "/data/dataoverview",
        icon: 'el-icon-document',
        meta: {
            i18n: 'FishNovel free novel android',
        },
        parentId: 3
    },
    // {
    //     label: "pc站",
    //     path: "/pcOperate/activeAndNews",
    //     icon: 'el-icon-document',
    //     meta: {
    //         i18n: 'FishNovel free novel android',
    //     },
    //     parentId: 4
    // },
    {
        label: "pc站",
        path: "/featuredFirst/home/banner",
        icon: 'el-icon-document',
        meta: {
            i18n: 'FishNovel free novel android',
        },
        parentId: 4
    },
    // {
    //   label: "数据",
    //   path: "/orangecat",
    //   icon: 'el-icon-document',
    //   meta: {
    //     i18n: 'FishNovel free novel android',
    //   },
    //   parentId: 4
    // },
]
/**FishNovel APP运营管理 */
const first = [{
        label: "推荐位管理",
        path: '/featured',
        icon: 'icon-caidan',
        children: [
            // {
            //     label: '启动弹窗',
            //     path: 'startpopup',
            //     component: 'views/orangecat/appoperation/featured/startpopup',
            // },
            // {
            //     label: '开屏推荐',
            //     path: 'openrecommend',
            //     component: 'views/orangecat/appoperation/featured/openrecommend',
            // },
            {
                label: '书馆推荐位',
                path: 'librariesrecommend',
                children: [{
                        label: "tab列表",
                        path: "tablist",
                        component: 'views/orangecat/appoperation/librariesrecommend/tablist'
                    },
                    {
                        label: "tab内容配置",
                        path: "tabcontent",
                        component: 'views/orangecat/appoperation/librariesrecommend/tablistcontent'
                    },
                    {
                        label: "常驻模块内容配置",
                        path: "modulebookrecommend",
                        component: 'views/orangecat/appoperation/librariesrecommend/modulebookrecommend'
                    },
                    {
                        label: "自定义模块内容配置",
                        path: "modulecustomerrecommend",
                        component: 'views/orangecat/appoperation/librariesrecommend/modulecustomerrecommend'
                    },
                    // {
                    //     label: "分类书籍模块内容配置",
                    //     path: "modulecategoryrecommend",
                    //     component: 'views/orangecat/appoperation/librariesrecommend/modulecategoryrecommend'
                    // },
                    {
                        label: "分类模块内容配置",
                        path: "modulecategoryinforecommend",
                        component: 'views/orangecat/appoperation/librariesrecommend/modulecategoryinforecommend'
                    },
                ]
            },
            {
                label: 'banner',
                path: 'bookstore',
                children: [{
                        label: '顶部banner',
                        path: 'topbanner',
                        component: 'views/orangecat/appoperation/featured/lightnovelbookstore/topbanner',
                    },
                    {
                        label: '任务banner',
                        path: 'taskbanner',
                        component: 'views/orangecat/appoperation/featured/lightnovelbookstore/taskbanner',
                    },
                    // {
                    //     label: '活动banner',
                    //     path: 'activitybanner',
                    //     component: 'views/orangecat/appoperation/featured/lightnovelbookstore/activitybanner',
                    // },
                    // {
                    // 	label: '风云人物',
                    // 	path: 'influential',
                    // 	component: 'views/orangecat/appoperation/featured/bookstorechoiceness/influential',
                    // },
                    // {
                    // 	label: '潜力佳作',
                    // 	path: 'recommendlist',
                    // 	component: 'views/orangecat/appoperation/featured/bookstorechoiceness/recommendList',
                    // },
                    // {
                    // 	label: '人气写手',
                    // 	path: 'popularwriting',
                    // 	component: 'views/orangecat/appoperation/featured/bookstorechoiceness/popularwriting',
                    // },
                    // {
                    // 	label: '宝藏新书',
                    // 	path: 'treasurebook',
                    // 	component: 'views/orangecat/appoperation/featured/bookstorechoiceness/treasurebook',
                    // },
                    // {
                    // 	label: '为你种草',
                    // 	path: 'asyougrowgrass',
                    // 	component: 'views/orangecat/appoperation/featured/bookstorechoiceness/asyougrowgrass',
                    // },
                    // {
                    // 	label: '风向标',
                    // 	path: 'windvane',
                    // 	component: 'views/orangecat/appoperation/featured/bookstorechoiceness/windvane',
                    // },

                ]
            },
            // {
            // 	label: '书馆-轻小说',
            // 	path: 'lightnovelbookstore',
            // 	children: [
            // 		{
            // 			label: '顶部banner',
            // 			path: 'topbanner',
            // 			component: 'views/orangecat/appoperation/featured/lightnovelbookstore/topbanner',
            // 		},
            // 		{
            // 			label: '活动banner',
            // 			path: 'activitybanner',
            // 			component: 'views/orangecat/appoperation/featured/lightnovelbookstore/activitybanner',
            // 		},
            // 		{
            // 			label: '男女频好书...',
            // 			path: 'Intothepitforgod',
            // 			component: 'views/orangecat/appoperation/featured/lightnovelbookstore/Intothepitforgod',
            // 		},
            // 		{
            // 			label: '爆款推荐',
            // 			path: 'nicebook',
            // 			component: 'views/orangecat/appoperation/featured/lightnovelbookstore/nicebook',
            // 		},
            // 		{
            // 			label: '专属阅读',
            // 			path: 'orangecatmumrecommend',
            // 			component: 'views/orangecat/appoperation/featured/lightnovelbookstore/orangecatmumrecommend',
            // 		},
            // 		{
            // 			label: '本周飙升榜',
            // 			path: 'soaringlist',
            // 			component: 'views/orangecat/appoperation/featured/lightnovelbookstore/soaringlist',
            // 		}
            // 	]
            // }, 
            {
                label: '阅读器',
                path: 'reader',
                children: [{
                        label: '完本页推荐位',
                        path: 'lastpage',
                        component: 'views/orangecat/appoperation/featured/reader/lastpage'
                    },
                    {
                        label: '退出阅读器推荐位',
                        path: 'exit',
                        component: 'views/orangecat/appoperation/featured/reader/exit'
                    },
                    {
                        label: '独立页书籍推荐位',
                        path: 'independent',
                        component: 'views/orangecat/appoperation/featured/reader/independent'
                    },
                    {
                        label: '广告解锁推荐书籍',
                        path: 'advert',
                        component: 'views/orangecat/appoperation/featured/reader/advert'
                    }
                ]
            },
            {
                label: '排行榜',
                path: 'rankinglist',
                component: 'views/orangecat/appoperation/featured/rankinglist/rankinglist',
            },
            {
                label: '热搜词',
                path: 'search',
                component: 'views/orangecat/appoperation/featured/trendingsearch',
            },
            {
                label: '书架',
                path: 'bookrack',
                children: [{
                        label: '新用户书架',
                        path: 'newuserrecommend',
                        component: 'views/orangecat/appoperation/featured/bookrack/newuserrecommend',
                    },
                    {
                        label: '书架推荐',
                        path: 'bookrackrecommend',
                        component: 'views/orangecat/appoperation/featured/bookrack/bookrackrecommend',
                    },
                    // {
                    //     label: '顶部推荐',
                    //     path: 'topbookrecommendation',
                    //     component: 'views/orangecat/appoperation/featured/bookrack/topbook/topbookrecommendation'
                    // },
                    // {
                    // 	label: '背景图',
                    // 	path: 'backgroundpic',
                    // 	component: 'views/orangecat/appoperation/featured/bookrack/backgroundpic/backgroundpic',
                    // },
                ]
            }
        ]
    },
    {
        label: "内容运营",
        path: '/orangecatcontent',
        icon: 'icon-caidan',
        children: [{
                label: '书籍管理',
                path: 'bookmanager',
                component: 'views/orangecat/appoperation/content/booksmanagement/booksmanagement',
            },
            {
                label: '书籍分类一级列表',
                path: 'bookCategory',
                component: 'views/orangecat/appoperation/content/booksmanagement/bookCategory'
            },
            {
                label: '书籍分类管理',
                path: 'bookCategoryManager',
                component: 'views/orangecat/appoperation/content/booksmanagement/bookCategoryManager'
            },
            {
                label: '导入记录',
                path: 'booksImport',
                component: 'views/orangecat/appoperation/content/booksmanagement/booksImport',
            },
            {
                label: '书籍机翻',
                path: 'booksMachineTurnover',
                component: 'views/orangecat/appoperation/content/booksmanagement/booksMachineTurnover',
            },
            {
                label: '书籍导出',
                path: 'bookslabel',
                component: 'views/orangecat/appoperation/content/booksmanagement/bookslabel',
            },
            // {
            //     label: '角色管理',
            //     path: 'rolemanager',
            //     component: 'views/orangecat/appoperation/content/rolemanagement',
            // },
            // {
            //     label: '创建动态',
            //     path: 'dynamicmanagement',
            //     component: 'views/orangecat/appoperation/content/dynamic/dynamicmanagement',
            // },
            // {
            //     label: '马甲管理',
            //     path: 'fakeusermanager',
            //     component: 'views/orangecat/appoperation/content/fakeuser/fakeusermanagment',
            // },
            // {
            //     label: '评论模板',
            //     path: 'commenttemplate',
            //     component: 'views/orangecat/appoperation/content/commenttemplate/commenttemplate',
            // },
            // {
            //     label: '热门标签管理',
            //     path: 'hot_tags',
            //     component: 'views/orangecat/appoperation/content/hot_tags',
            // },
            {
                label: "帮助与反馈",
                path: "helpandfeedback",
                children: [{
                    label: "意见反馈",
                    path: "opinionfeedback",
                    component: "views/orangecat/appoperation/content/helpandfeedback/opinionfeedback"
                }]
            },
        ]
    },
    {
        label: "会员充值",
        path: '/orangecatviprecharge',
        icon: 'icon-caidan',
        children: [{
                label: '书币充值配置',
                path: 'rechargeamount ',
                component: 'views/orangecat/appoperation/viprecharge/rechargeamount',
            },
            // {
            //     label: 'Plus会员充值',
            //     path: 'viprecharge',
            //     component: 'views/orangecat/appoperation/viprecharge/viprecharge',
            // },
            {
                label: '会员购买配置',
                path: 'premiumviprecharge',
                component: 'views/orangecat/appoperation/viprecharge/premiumviprecharge',
            },
            {
                label: '充值页会员排序',
                path: 'bookCurrency',
                component: 'views/orangecat/appoperation/viprecharge/bookCurrency',
            },
            {
                label: '打赏配置',
                path: 'rewardconfig',
                component: 'views/orangecat/appoperation/viprecharge/rewardconfig',
            },
            {
                label: '提现配置',
                path: 'withdrawalConfig',
                component: 'views/orangecat/appoperation/viprecharge/withdrawalConfig',
            },
            // {
            //     label: '限时特惠配置',
            //     path: 'timeLimitDiscount',
            //     component: 'views/orangecat/appoperation/viprecharge/timeLimitDiscount',
            // },
            {
                label: '阅读器盲盒弹窗配置',
                path: 'readConfigBlinBox',
                component: 'views/orangecat/appoperation/viprecharge/readConfigBlinBox',
            },
            {
                label: 'Bonus兑换',
                path: 'exchange',
                component: 'views/orangecat/appoperation/viprecharge/exchange',
            },
            {
                label: '弹窗配置后台',
                path: 'firstPurchase',
                component: 'views/orangecat/appoperation/viprecharge/firstPurchase',
            }

        ]
    },
    {
        label: "任务",
        path: '/orangecattask',
        icon: 'icon-caidan',
        children: [{
                label: '签到',
                path: 'checkin',
                component: 'views/orangecat/appoperation/task/checkin',
            },
            {
                label: '阅读时长领书币',
                path: 'readTimeGet',
                component: 'views/orangecat/appoperation/task/readTimeGet',
            },
            {
                label: '任务库',
                path: 'taskLibrary',
                component: 'views/orangecat/appoperation/task/taskLibrary',
            },
            {
                label: '任务列表',
                path: 'taskList',
                component: 'views/orangecat/appoperation/task/taskList',
            },
            {
                label: '抽奖概率配置',
                path: 'userLuckDraw',
                component: 'views/orangecat/appoperation/task/userLuckDraw',
            }
        ]
    },
    {
        label: "产品配置",
        path: '/orangecatproductconfig',
        icon: 'icon-caidan',
        children: [{
                label: '测试工具',
                path: 'testtool',
                children: [{
                        label: '用户信息查询',
                        path: 'userinfoquery',
                        component: 'views/orangecat/appoperation/productconfig/testtool/userinfoquery'
                    },

                    {
                        label: '1分钱充值',
                        path: 'redcentrecharge',
                        component: 'views/orangecat/appoperation/productconfig/testtool/redcentrecharge'
                    },
                    {
                        label: '书币变更',
                        path: 'mewmoneychange',
                        component: 'views/orangecat/appoperation/productconfig/testtool/mewmoneychange'
                    },
                    {
                        label: '鱼摆摆变更',
                        path: 'orangejuanchange',
                        component: 'views/orangecat/appoperation/productconfig/testtool/orangejuanchange'
                    },
                    {
                        label: '账号删除',
                        path: 'accountdelete',
                        component: 'views/orangecat/appoperation/productconfig/testtool/accountdelete'
                    },
                ]
            },
            {
                label: '推送工具',
                path: 'pushtool',
                children: [
                    {
                        label: '书籍推送',
                        path: 'pushbook',
                        component: 'views/orangecat/appoperation/productconfig/pushtool/pushbook'
                    },
                ]
            },
            // {
            //     label: '用户群组配置',
            //     path: 'userGroupConfig',
            //     component: 'views/orangecat/appoperation/productconfig/testtool/userGroupConfig'
            // },
            {
                label: '新用户启动跳转（渠道）',
                path: 'newuserstartsjump',
                component: 'views/orangecat/appoperation/productconfig/newuserstartsjump'
            },
            {
                label: '广告用户启动跳转',
                path: 'aduserstartsjump',
                component: 'views/orangecat/appoperation/productconfig/aduserstartsjump'
            },
            {
                label: '功能开关配置',
                path: 'switchConfig',
                component: 'views/orangecat/appoperation/productconfig/switchConfig'
            },
            {
                label: '多语言文本配置',
                path: 'multilingualTxt',
                component: 'views/orangecat/appoperation/productconfig/multilingualTxt'
            },
            {
                label: '广告解锁配置',
                path: 'adUnlockConfig',
                component: 'views/orangecat/appoperation/productconfig/adUnlockConfig'
            },
            {
                label: '渠道配置',
                path: 'channelManage',
                component: 'views/orangecat/appoperation/productconfig/channelManage'
            },
            {
                label: '新用户天数',
                path: 'newUserConfig.vue',
                component: 'views/orangecat/appoperation/productconfig/newUserConfig'
            }
            // {
            //   label: 'VIP专属客服配置',
            //   path: 'vipexclusiveserviceconfig',
            //   component: 'views/orangecat/appoperation/vipexclusiveserviceconfig'
            // }
        ]
    },
    {
        label: '导出',
        path: '/export',
        icon: 'icon-caidan',
        children: [{
            label: '导出下载',
            path: 'exportDownload',
            component: 'views/orangecat/data/export/exportDownload',
        }]
    }
]

/**FishNovel 广告运营管理 */
const orgad = [{
    label: '广告运营',
    path: '/ad',
    children: [{
            label: '广告位id设置',
            path: 'ad_id_config',
            component: 'views/orangecat/adoperation/adidconfig'
        },
        {
            label: '广告展示配置',
            path: 'exhibition',
            children: [{
                label: '广告配置',
                path: 'adshow',
                component: 'views/orangecat/adoperation/adshow/adshowconfig'
            }]
        },
        {
            label: '广告位规则设置',
            path: 'rule',
            children: [{
                label: '阅读器广告页数配置',
                path: 'readeradpagenum',
                component: 'views/orangecat/adoperation/adruleset/readeradpageconfig'
            }]
        },
        {
            label: '开始展示广告章节配置',
            path: 'ad_unlock',
            children: [{
                label: '开始展示广告章节配置',
                path: 'readavoidad',
                component: 'views/orangecat/adoperation/adunlock/readavoidad'
            }]
        },
        {
            label: '渠道免广告设置',
            path: 'channelavoidad',
            component: 'views/orangecat/adoperation/channelavoidad/channelavoidad'
        },
        {
            label: '配置克隆',
            path: 'cloneconfig',
            component: 'views/orangecat/adoperation/cloneadconfig'
        },
    ]

}]

/**FishNovel 版本管理 */
const second = [{
        label: "财务审核",
        path: "/orangefinancialaudit",
        children: [{
            label: "零钱提现",
            path: "moneywithdraw",
            component: 'views/orangecat/auditor/financialaudit/moneywithdraw'
        }]
    },
    {
        label: "内容审核",
        path: "/orangecontentaudit",
        children: [{
                label: "用户头像",
                path: "userheadphoto",
                component: 'views/orangecat/auditor/contentaudit/userheadphoto'
            },
            {
                label: "用户昵称",
                path: "usernickname",
                component: 'views/orangecat/auditor/contentaudit/usernickname'
            },
            {
                label: "书籍评论(先发后审)",
                path: "bookcomment",
                component: 'views/orangecat/auditor/contentaudit/bookcomment'
            },
            {
                label: "动态评论(先发后审)",
                path: "dynamiccomment",
                component: 'views/orangecat/auditor/contentaudit/dynamiccomment'
            },
            // {
            // 	label: "插画评论(先发后审)",
            // 	path: "insetcomment",
            // 	component: 'views/orangecat/auditor/contentaudit/insetcomment'
            // },
            // {
            // 	label: "插画(先发后审)",
            // 	path: "illustration",
            // 	component: 'views/orangecat/auditor/contentaudit/illustration'
            // },
            {
                label: "动态",
                path: "dynamic",
                component: 'views/orangecat/auditor/contentaudit/dynamic'
            },
            {
                label: "作品",
                path: "works",
                children: [{
                        label: "作品基本信息",
                        path: "book",
                        component: 'views/orangecat/auditor/contentaudit/works/works'
                    },
                    {
                        label: "作品章节信息",
                        path: "bookchapter",
                        component: 'views/orangecat/auditor/contentaudit/works/chapter'
                    }
                ]
            },
            // {
            // 	label: '家乡话flag',
            // 	path: '/flagmenager',
            // 	icon: 'icon-caidan',
            // 	children: [{
            // 			label: "flag",
            // 			path: "flag",
            // 			component: 'views/orangecat/auditor/contentaudit/flag/flag'
            // 		},
            // 		{
            // 			label: "flag评论",
            // 			path: "flagcomment",
            // 			component: 'views/orangecat/auditor/contentaudit/flag/comment'
            // 		}
            // 	],
            // }
        ]
    },
    // {
    // 	label: "身份审核",
    // 	path: "/identityverification",
    // 	children: [{
    // 		label: "作者",
    // 		path: "author",
    // 		component: 'views/orangecat/auditor/identityverification/author'
    // 	}
    // 	// , {
    // 	// 	label: "插画师",
    // 	// 	path: "illustrator",
    // 	// 	component: 'views/orangecat/auditor/identityverification/illustrator'
    // 	// }
    // ]
    // },
    {
        label: "危险词汇",
        path: "/orangedangerword",
        children: [{
            label: "危险词汇",
            path: "dangerword",
            component: 'views/orangecat/auditor/dangerword/dangerword'
        }]
    },
]

/**FishNovel 审核管理 */
const three = [{
        label: '渠道管理',
        path: '/orangecatversionpromotion/channelmanager',
        icon: 'icon-caidan',
        component: 'views/orangecat/appversionmanager/channelnmanager'
    },
    {
        label: '打包管理',
        path: '/orangecatversionpromotion/packagemanager',
        icon: 'icon-caidan',
        component: 'views/orangecat/appversionmanager/packagemanager'
    },
    {
        label: '版本管理',
        path: '/orangecatversionpromotion/versionmanager',
        icon: 'icon-caidan',
        component: 'views/orangecat/appversionmanager/versionmanager'
    },

]

/**FishNovel 数据管理 */
const four = [{
        label: '产品',
        path: '/data',
        icon: 'icon-caidan',
        children: [
            {
                label: '实时新增数据（小时）',
                path: 'realtimenewdata',
                component: 'views/orangecat/data/product/realtimenewdata',
            },
            {
                label: '新增阅读概况',
                path: 'dataoverview',
                component: 'views/orangecat/data/product/dataoverview',
            },
            {
                label: '投入产出',
                path: 'inputOutput',
                component: 'views/orangecat/data/product/inputOutput',
            },
            {
                label: 'DAU阅读情况',
                path: 'readingsituation',
                component: 'views/orangecat/data/product/readingsituation',
            },
            {
                label: '新增留存率',
                path: 'retention',
                component: 'views/orangecat/data/product/retention',
            },
            // {
            //     label: '新增阅读情况',
            //     path: 'newreading',
            //     component: 'views/orangecat/data/product/newreading',
            // },
            // {
            //     label: '每日各渠道新增情况',
            //     path: 'realtimeaddition',
            //     component: 'views/orangecat/data/product/realtimeaddition',
            // },
            // {
            //     label: '每日唤醒用户数据',
            //     path: 'wakeupusers',
            //     component: 'views/orangecat/data/product/wakeupusers',
            // },
            // {
            //     label: '搜索下载统计',
            //     path: 'statistics',
            //     component: 'views/orangecat/data/product/searchdownloadstatistics',
            // },
        ]
    },
    {
        label: '书籍',
        path: '/book',
        icon: 'icon-caidan',
        children: [{
                label: '每日阅读统计',
                path: 'everydayread',
                component: 'views/orangecat/data/book/everydayread/book',
            },
            {
                label: '累计阅读统计',
                path: 'totalread',
                component: 'views/orangecat/data/book/totalread/book',
            },
            {
                label: '付费用户每日阅读统计',
                path: 'freeeverydayread',
                component: 'views/orangecat/data/book/freeeverydayread/book',
            },
            {
                label: '新用户每日阅读统计',
                path: 'newuserread',
                component: 'views/orangecat/data/book/newuserread/book',
            },
            // {
            //     label: '付费用户累计阅读统计',
            //     path: 'cumulativereadingstatisticspay',
            //     component: 'views/orangecat/data/book/cumulativereadingstatisticspay/cumulativereadingstatisticspay',
            // },
            // {
            //     label: '书籍阅读用户分布',
            //     path: 'distributionreadingusers',
            //     component: 'views/orangecat/data/book/distributionreadingusers',
            // },
            // {
            //     label: '新增阅读数据',
            //     path: 'newreadingdata',
            //     component: 'views/orangecat/data/book/newreadingdata',
            // },
            // {
            //     label: '新增次日留存章节数',
            //     path: 'newaddyesterdayretentionchapternumber',
            //     component: 'views/orangecat/data/book/newaddyesterdayretentionchapternumber',
            // },
            // {
            //     label: '书籍留存',
            //     path: 'bookretention',
            //     component: 'views/orangecat/data/book/bookretention',
            // },
            // {
            //     label: '新用户书籍留存',
            //     path: 'newuserbookretention',
            //     component: 'views/orangecat/data/book/newuserbookretention',
            // },
            // {
            //     label: '章节留存',
            //     path: 'chapterretention',
            //     component: 'views/orangecat/data/book/chapterretention',
            // },
            // {
            //     label: '书籍数据搜索',
            //     path: 'bookdatasearch',
            //     component: 'views/orangecat/data/book/bookdatasearch',
            // },
            // {
            //   label: '听书统计',
            //   path: 'listeningstatistics',
            //   component: 'views/orangecat/data/book/listeningstatistics',
            // },
            // {
            //   label: '听书统计明细',
            //   path: 'listeningstatisticsdetailed',
            //   component: 'views/orangecat/data/book/listeningstatisticsdetailed',
            // },

            // {
            //     label: '阅读人数统计',
            //     path: 'readpeoplenum',
            //     component: 'views/orangecat/data/book/new/readpeoplenum',
            // },
            // {
            //     label: '收藏量统计',
            //     path: 'collectnum',
            //     component: 'views/orangecat/data/book/new/collectnum',
            // },
            // {
            //     label: '打赏额度统计',
            //     path: 'exceptionallines',
            //     component: 'views/orangecat/data/book/new/exceptionallines',
            // },
            // {
            //     label: '全本评论统计',
            //     path: 'bookcomment',
            //     component: 'views/orangecat/data/book/new/bookcomment',
            // },
            // {
            //     label: '书籍首章数据',
            //     path: 'bookfirstchapter',
            //     component: 'views/orangecat/data/book/new/bookfirstchapter',
            // },
            // {
            //     label: '章节次日留存',
            //     path: 'chapternextdaykeep',
            //     component: 'views/orangecat/data/book/new/chapternextdaykeep',
            // },
            // {
            //     label: '阅读完成数据',
            //     path: 'readfinishdata',
            //     component: 'views/orangecat/data/book/new/readfinishdata',
            // },
            {
                label: '书籍订阅数据',
                path: 'booksubscription',
                component: 'views/orangecat/data/book/new/booksubscription',
            },
            {
                label: '书籍每日订阅',
                path: 'bookeverydaysubscription',
                component: 'views/orangecat/data/book/everydaysubs/bookeverydaysubscription',
            },
        ]
    },
    // {
    //     label: '广告',
    //     path: '/orangecat/data/ad',
    //     icon: 'icon-caidan',
    //     children: [{
    //             label: '广告位数据',
    //             path: 'addata',
    //             component: 'views/orangecat/data/ad/addata',
    //         },
    //         {
    //           label: '广告位数据',
    //           path: 'addata',
    //           component: 'views/peas/data/ad/addatanew',
    //         },
    //         {
    //             label: '广告位数据-按时',
    //             path: 'realtimeaddata',
    //             component: 'views/orangecat/data/ad/realtimeaddata',
    //         }
    //     ]
    // },
    {
        label: '用户',
        path: '/user',
        icon: 'icon-caidan',
        children: [
            // {
            //     label: 'Plus会员阅读数据',
            //     path: 'readingdatavip',
            //     component: 'views/orangecat/data/user/readingdatavip',
            // },
            // {
            //     label: 'Premium会员阅读数据',
            //     path: 'readingdatapremiumvip',
            //     component: 'views/orangecat/data/user/readingdatapremiumvip',
            // },
            // {
            //     label: 'Plus会员购买数据',
            //     path: 'purchasedatavip',
            //     component: 'views/orangecat/data/user/purchasedatavip',
            // },
            {
                label: '付费用户统计',
                path: 'purchasedatapremiumvip',
                component: 'views/orangecat/data/user/purchasedatapremiumvip',
            },
            {
                label: '用户财务数据',
                path: 'userfinancialdata',
                component: 'views/orangecat/data/user/userfinancialdata',
            },
            {
                label: '阅读数据查询',
                path: 'userreaddata',
                component: 'views/orangecat/data/user/userreaddata',
            },
        ]
    },
    {
        label: '充值',
        path: '/recharge',
        icon: 'icon-caidan',
        children: [
            // {
            //     label: '每日Plus会员购买统计',
            //     path: 'dailymemberpurchasestatistics',
            //     component: 'views/orangecat/data/recharge/dailymemberpurchasestatistics',
            // },
            {
                label: '每日Premium会员购买统计',
                path: 'dailypremiummemberpurchasestatistics',
                component: 'views/orangecat/data/recharge/dailypremiummemberpurchasestatistics',
            },
            {
                label: '每日喵币充值数据',
                path: 'dailymeowcoinrechargestatistics',
                component: 'views/orangecat/data/recharge/dailymeowcoinrechargestatistics',
            },
            {
                label: '交易订单明细',
                path: 'transactionorderdetails',
                component: 'views/orangecat/data/recharge/transactionorderdetails',
            },
        ]
    },
    // {
    //     label: '活动',
    //     path: '/active',
    //     icon: 'icon-caidan',
    //     children: [{
    //         label: '天天领红包',
    //         path: 'newYeardata',
    //         component: 'views/orangecat/data/active/newYeardata',
    //     }]
    // },
    {
        label: '导出',
        path: '/export',
        icon: 'icon-caidan',
        children: [{
            label: '导出下载',
            path: 'exportDownload',
            component: 'views/orangecat/data/export/exportDownload',
        }]
    }
]

/**橘子pc站 */
const five = [{
        label: 'PC站推荐位管理',
        path: '/featuredFirst',
        children: [{
            label: 'Home',
            path: 'home',
            children: [{
                    label: 'Banner',
                    path: 'banner',
                    component: 'views/orangecat/pc/featuredFirst/home/banner'
                },
                {
                    label: 'Exclusive Selection',
                    path: 'exclusive',
                    component: 'views/orangecat/pc/featuredFirst/home/exclusive'
                },
                {
                    label: 'Rankings',
                    path: 'rankings',
                    component: 'views/orangecat/pc/featuredFirst/home/rankings'
                },
                {
                    label: 'Genres',
                    path: 'genres',
                    component: 'views/orangecat/pc/featuredFirst/home/genres'
                },
                {
                    label: 'Boy/Girl Favorite',
                    path: 'favorite',
                    component: 'views/orangecat/pc/featuredFirst/home/favorite'
                }
            ]
        }, ]
    }, {
        label: 'PC站运营',
        path: '/pcOperate',
        icon: 'icon-caidan',
        children: [{
                label: '最新资讯和征文活动',
                path: 'activeAndNews',
                component: 'views/orangecat/pc/pcOperate/activeAndNews',
            },
            {
                label: '轮播图管理',
                path: 'banner',
                component: 'views/orangecat/pc/pcOperate/banner',
            },
        ]
    },
    {
        label: "站内信管理",
        path: '/message',
        children: [{
                label: '推送消息管理',
                path: 'pushMessage',
                component: 'views/orangecat/pc/message/pushMessage'
            },
            {
                label: '作者咨询管理',
                path: 'authorCons',
                component: 'views/orangecat/pc/authorCons/index'
            },
            {
                label: '自动回复模板',
                path: 'autoTemplate',
                component: 'views/orangecat/pc/autoTemplate/index'
            }
        ]
    },
    {
        label: "作者管理",
        path: '/authorAdmin',
        children: [{
            label: '作者列表',
            path: 'pushMessage',
            component: 'views/orangecat/pc/authorAdmin/index'
        }, 
        {
            label: '签约书籍列表',
            path: 'signBook',
            component: 'views/orangecat/pc/authorAdmin/signBook'
        },]
    },
    {
        label: "首页管理",
        path: '/homeAdmin',
        children: [{
            label: '首页推荐位配置',
            path: 'recommendHome',
            component: 'views/orangecat/pc/homeAdmin/index'
        }, ]
    }

]
// FishNovel 菜单end


export default ({
    mock
}) => {
    if (!mock) return;
    // let menu = [first, second, three, four, firstdd, seconddd, threedd, fourdd, fivedd, orgad,fourfishball];
    let menu = [
        first, second, three, four, five
    ];
    Mock.mock('/user/getMenu', 'get', (res) => {
        let body = JSON.parse(res.body);
        return {
            data: menu[body.type] || []
        }
    })
    let topMenu = [top]
    Mock.mock('/user/getTopMenu', 'get', (res) => {
        let body = JSON.parse(res.body);
        // if(body.value == null)
        return {
            data: topMenu[body.value]
        }
    })

}