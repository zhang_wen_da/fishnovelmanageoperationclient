import request from '@/router/axios';
import { baseUrl, catUrl } from '@/config/env';
export const loginByUsername = (username, password, code, redomStr) => request({
    url: catUrl + '/user/loginTwo',
    method: 'post',
    meta: {
        isToken: true
    },
    // headers:{
    //     "Content-Type" : 'application/x-www-form-urlencoded'
    // },
    data: {
        username,
        password,
        code,
        redomStr
    }
})

export const getUserInfo = () => request({
    url: catUrl + '/user/getUserInfo',
    method: 'get'
});

export const refeshToken = () => request({
    url: baseUrl + '/user/refesh',
    method: 'post'
})

export const getMenu = (type = 0, appType) => request({
    url: baseUrl + '/user/getMenu',
    method: 'get',
    data: {
        type, appType
    }
});

export const getTopMenu = (value) => request({
    url: baseUrl + '/user/getTopMenu',
    method: 'get',
    data: {
        value
    }
});

export const sendLogs = (list) => request({
    url: baseUrl + '/user/logout',
    method: 'post',
    data: list
})

export const logout = () => request({
    url: baseUrl + '/user/logout',
    meta: {
        isToken: false
    },
    method: 'get'
})