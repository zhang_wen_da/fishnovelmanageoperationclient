
/** 橘子猫请求封装 */
import { common as commoncat } from "@/api/cat/httpRequest";
import {
  Message
} from 'element-ui';

/** 统一的导出接口(橘子猫) */
export const handleExcelCat = (parentExport,requestUrl,params,title,pageSize,column,handleAllArr,alternativeArr) =>{
      myExport(commoncat,parentExport,requestUrl,params,title,pageSize,column,handleAllArr,alternativeArr)
}

/**
 * 导出封装
 * @param {*} mycommon 请求封装
 * @param {*} parentExport 调用页面的this.$export对象
 * @param {*} requestUrl 请求数据接口路径
 * @param {*} params 请求条件
 * @param {*} title 导出文件名
 * @param {*} pageSize 请求条数
 * @param {*} column 导出列数据
 * @param {*} handleAllArr 需要处理的下拉列表数据
 * @param {*} alternativeArr 单独处理的下拉列表数据
 */
const myExport = async (mycommon,parentExport,requestUrl,params,title,pageSize,column,handleAllArr,alternativeArr)=>{
      if(pageSize>25000){
        Message.info("不可导出过大数据(最大25000条数据),当前导出数据("+pageSize+")");
        return;
      }else{
        Message.info("请耐心等待导出完毕(0分钟-5分钟)")
      }
      //删除不需要的搜索数据,下拉列表的中文字
      for(let key in params){
        if(key.startsWith("$")){
          delete params[key]
        }
      }
      //根据条件查询列表,不分页 
      let response = await mycommon( requestUrl, params, {pageNum:1, pageSize:pageSize,currentPage:1}, "POST" );
      if(response==null){
        // this.$message.error("无数据")
        return;
      }
      let data = response.data;
      if(data==null || data.length==0){
        // this.$message.error("无数据")
        return;
      }else {
        let dataTwo = data.records;
        if(dataTwo!=null && dataTwo.length>=0){
          data = dataTwo;
        }
      }
      //特殊定制
      if(requestUrl=='/catPayOrder/pageAndParam'){
        data.forEach(e=>{
          e.create_time_2 = e.create_time
          let time = new Date(e.create_time);
          e.create_time = time.getFullYear()+"-"+(time.getMonth()+1)+'-'+time.getDate()
          if(e.business_type == 1){
            if(e.vip_type==0){
              e.sss = "月卡"
            }else if(e.vip_type==1){
              e.sss = "季卡"
            }else {
              e.sss = "年卡"
            }
          }else{
            e.sss = e.total_fee+'(元)'
          }
        })
      }
      //逻辑处理需要展示的下拉列表数据
      if(handleAllArr!=null){
        //下拉列表的字段名称
        let selectNameArr = handleAllArr.selectNameArr;
        //下拉列表对应的字段名称
        let sourceNameArr = handleAllArr.sourceNameArr;
        //下拉列表对应的数据字段名称
        let sourceValueArr = handleAllArr.sourceValueArr;
        //下拉列表在什么位置 - 即索引
        let handleIndexArr = handleAllArr.handleIndexArr;
        handleIndexArr.forEach((handleIndex,index) =>{
          column[handleIndex].dicData.forEach(eOne =>{
            data.forEach(eTwo =>{
              if(eTwo[selectNameArr[index]] == eOne[sourceNameArr[index]]){
                eTwo[selectNameArr[index]] = eOne[sourceValueArr[index]].match(/[\u4e00-\u9fa5]+/ig)
              }
            })
          })
        })
      }
   
      //一些单独处理的select框 : 如:site
      if(alternativeArr!=null){
        alternativeArr.forEach(eOne =>{
          data.forEach(eTwo =>{
            let prop = column[eOne].prop;
            let item = column[eOne].dicData.filter(e => e.value == eTwo[prop])[0];
            if(item!=null){
              eTwo[prop] = item.label;
            }
          })
        })
      }
      //删除不需要导出的字段,操作完后删除,避免出现错乱问题
      let deleteIndexArr = [];
      column.forEach((item,index) =>{
        if(item.hide){
			// isonlyhide 添加这个字段和hide同级 则仅对页面内容进行隐藏不进行导出删除操作
			if(item.isonlyhide){
				
			}else{
				deleteIndexArr.push(index);
			}
        }
      })
      if(deleteIndexArr!=null && deleteIndexArr.length>0){
        let i = false;
        deleteIndexArr.forEach(delIndex =>{
          if(delIndex==0){
            i=true;
            column.splice(delIndex,1)
          }else{
            if(i){
              column.splice(delIndex-1,1)
            }else{
              i=true;
              column.splice(delIndex,1)
            }
          }
        })
      }
      //导出
      parentExport.excel({
        title: title || new Date(),
        columns: column,
        data: data
      });
}



