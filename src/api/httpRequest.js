import request from '@/router/axios';
import { ddUrl } from '@/config/env'
import {
    Message
} from 'element-ui';



/**crud */
// export const common = (param, url, method) => request({
//     url: ddUrl + `/adoperation/${url}`,
//     method: method,
//     data: {
//         ...param
//     }
// })

export const del = (param, url, method) => request({
    url: ddUrl + `/adoperation/${url}`,
    method: method,
    data: {
        ...param
    }
})


export const common = async (url, param, page, method = "GET") => {
    if (page) {
        param = Object.assign(
            {
                pageNum: page.currentPage,
                pageSize: page.pageSize,
            },
            param
        );
    }
    let response = await request({
        url: ddUrl + url,// +(method=="DELETE"&&param==null?"/"+param:""),
        method: method,
        params: method == "GET" ? param : null,
        data: method == "GET" ? null : param
    })

    // 处理返回值结果
    let data = response.data;
    if (data.code == 1) {
        return Result.success(data)
    }
    return null;
}


class Result {
    constructor(data, page, code) {
        this.data = data
        this.page = page
        this.code = code
    }

    static success (result) {
        let page = {}
        let data
        let code
        if (result.total) {
            // 表示是分页查询
            page.total = result.total
            page.pageSize = result.size;
            page.currentPage = result.current;
            data = result.rows
        } else {
            // Message.success("操作成功");
            data = result.source
        }
        code = result.code
        if (!data) {
            data = []
        }
        return new Result(data, page,code)
    }
}

