
//豆豆启动弹窗controller路径
let appConfigPopup = "/appConfigPopup"
let appCOnfigModuleList = "/modulelist"
let appConfigBanner = "/appConfigBanner"
let appConfigModulePosition = "/moduleposition"
let appConfigModuleOrder = "/moduleorder"
let appConfigHighBookList = "/appconfighighbooklist"
let appConfigModuleOnlineOrder = "/onlinemodulelist"

export {
    appConfigPopup,
    appCOnfigModuleList,
    appConfigBanner,
    appConfigModulePosition,
    appConfigModuleOrder,
    appConfigHighBookList,
    appConfigModuleOnlineOrder
}