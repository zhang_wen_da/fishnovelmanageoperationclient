import request from '@/router/axios';
import { catUrl } from '@/config/env'
import {
    Message
} from 'element-ui';
/**
 * 查询 查询数据列表
 */
export const getAppStatisticsAdList = (param) => request({
    url: catUrl + "/adoperation/getAppStatisticsAdList",
    method: "POST",
    data: {
        ...param
    }
})

/**查询指定的广告位列表 */
export const getAdListByTypeName = (param) => request({
    url: catUrl + "/adoperation/getAdListByTypeName",
    method: "POST",
    data: {
        ...param
    }
})
/**添加广告位 */
export const addReadAd = (param) => request({
    url: catUrl + "/adoperation/addReadAd",
    method: "POST",
    data: {
        ...param
    }
})
/**删除广告位 */
export const deleteAd = (param) => request({
    url: catUrl + "/adoperation/deleteAd/" + param,
    method: "DELETE"
})
/**删除广告位 */
export const deleteAppConfigAd = (param) => request({
    url: catUrl + "/adoperation/deleteAppConfigAd/" + param,
    method: "DELETE"
})
/**更新广告位规则 */
export const updateAdById = (param) => request({
    url: catUrl + "/adoperation/updateAdById",
    method: "PUT",
    data: {
        ...param
    }
})
/**查询有声小说付费章节配置 */
export const audioBookPayChapterConfig = () => request({
    url: catUrl + "/adoperation/audioBookPayChapterConfig",
    method: "GET"
})
/**更新有声小说付费章节配置 */
export const updateAudioBookPayChapterConfig = (param) => request({
    url: catUrl + "/adoperation/updateAudioBookPayChapterConfig",
    method: "PUT",
    data: {
        ...param
    }
})
/**查询渠道免广告列表 */
export const getAppConfigChannelAdList = (param) => request({
    url: catUrl + "/adoperation/getAppConfigChannelAdList",
    method: "POST",
    data: {
        ...param
    }
})
/**修改渠道免广告 */
export const updateAppConfigChannelAd = (param) => request({
    url: catUrl + "/adoperation/updateAppConfigChannelAd",
    method: "PUT",
    data: {
        ...param
    }
})
/**添加渠道免广告 */
export const addAppConfigChannelAd = (param) => request({
    url: catUrl + "/adoperation/addAppConfigChannelAd",
    method: "POST",
    data: {
        ...param
    }
})
/**开启关闭渠道免广告 */
export const closeAndOpenConfigChannelAd = (id, type) => request({
    url: catUrl + "/adoperation/closeAndOpenConfigChannelAd/" + id + "/" + type,
    method: "POST"
})


/**删除渠道免广告 */
export const deleteAppConfigChannelAd = (param) => request({
    url: catUrl + "/adoperation/deleteAppConfigChannelAd/" + param,
    method: "DELETE"
})
/**查询广告供货商 */
export const getAdProvider = (positionId, type) => request({
    url: catUrl + "/adoperation/getAdProvider/" + positionId + "/" + type,
    method: "GET",

})
/**查询广告位置名称 */
export const getAdPositionList = () => request({
    url: catUrl + "/adoperation/getAdPositionList",
    method: "GET",

})
/**查询广告主名称 */
export const getAdProviderList = () => request({
    url: catUrl + "/adoperation/getAdProviderList",
    method: "GET"
})
/**添加广告位配置 */
export const addConfigAd = (param) => request({
    url: catUrl + "/adoperation/addConfigAd",
    method: "POST",
    data: {
        ...param
    }
})
/**获取广告位id配置列表 */
export const getAppConfigAdLists = (param) => request({
    url: catUrl + "/adoperation/getAppConfigAdLists",
    method: "POST",
    data: {
        ...param
    }
})
/**开启或关闭广告配置 */
export const closeAndOpenConfigAd = (id, type) => request({
    url: catUrl + "/adoperation/closeAndOpenConfigAd/" + id + "/" + type,
    method: "POST"
})
/**获取广告详情 */
export const getConfigAdById = (id) => request({
    url: catUrl + "/adoperation/getConfigAdById/" + id,
    method: "GET"
})
/**修改广告位配置 */
export const updateConfigAd = (param) => request({
    url: catUrl + "/adoperation/updateConfigAd",
    method: "PUT",
    data: {
        ...param
    }
})

export const copy = (param) => request({
    url: catUrl + "/copy/copyConfig",
    method: "PUT",
    data: {
        ...param
    }
})