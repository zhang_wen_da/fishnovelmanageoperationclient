import request from '@/router/axios';
import {catUrl} from '@/config/env' 
import {
    Message
} from 'element-ui';

/**
 * 查询 指定广告位
 */
export const getadReadList = (param) => request({
    url: catUrl + "/adoperation/getAdListByTypeName",
    method: "POST",
    data : {
        ...param
    }
})
/**
 * 删除 指定广告位
 */
export const deleteAd = (param) => request({
    url: catUrl + "/adoperation/deleteAd/"+param,
    method: "DELETE",
})

/**
 * 更新 指定广告位
 */
export const updateAdById = (param) => request({
    url: catUrl + "/adoperation/updateAdById",
    method: "PUT",
    data : {
        ...param
    }
})
/**
 *  添加 指定广告位
 */
export const addReadAd = (param) => request({
    url: catUrl + "/adoperation/addReadAd",
    method: "POST",
    data : {
        ...param
    }
})

// /**
//  * 修改书籍
//  * @param {*} param 
//  */
// export const updateBook = (param) => request({
//     url: ddUrl + "/book/updateBook",
//     method: "POST",
//     data : {
//         ...param
//     }
// })