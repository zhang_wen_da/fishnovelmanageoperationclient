class BaseUrl {
    constructor(baseUrl) {
        this.baseUrl = baseUrl;
        this.pageAndParam = this.baseUrl + "/pageAndParam"
        this.update = this.baseUrl + "/update"
        this.save = this.baseUrl + "/save"
        this.page = this.baseUrl + "/page"
        this.delete = this.baseUrl + "/delete"
        this.updateAll = this.baseUrl + "/updateAll"
    }
    addOther(key, value) {
        this[key] = this.baseUrl + value
        return this
    }
}

/** 橘子猫 start */

/**版本 */
export const catversion = new BaseUrl("/selectVersion")

/** 文件上传 */
export const uploadFile = new BaseUrl("/file").addOther("uploadFile", "/uploadFile")

/** APP运营/推荐位管理/启动弹窗 */
export const appPopup = new BaseUrl("/appPopup").addOther("getSkippaheType", "/getSkippaheType")

/** APP运营/推荐位管理/开屏推荐 */
export const appConfigOpenRecommend = new BaseUrl("/appConfigOpenRecommend")

/**
 * app运营/推荐位管理/tab列表
 */
export const appConfigTab = new BaseUrl("/appConfigTab").addOther("setDefault", "/setDefault").addOther('selectTabListByAppVersion', '/selectTabListByAppVersion').addOther("setIsOnline", "/setIsOnline")
    // /userGroup/getGroupList
    //获取用户群组

export const getGroupList = new BaseUrl("/userGroup").addOther('getGroupList', '')
    //用户群组配置
export const userGroup = new BaseUrl("/userGroup")

/**
 * app运营/推荐位管理/tab内容
 */
export const appConfigModuleOrder = new BaseUrl("/appConfigModuleOrder").addOther('setIsOnline', '/setIsOnline')

/**
 * app运营/推荐位管理/模块排序
 */
export const appConfigModule = new BaseUrl("/appConfigModule").addOther("selectIsOrderList", "/selectIsOrderList").addOther('selectBookCommendModuleList', '/selectBookCommendModuleList')

/**
 * app运营/推荐位管理/模块化书籍推荐
 */
export const appConfigBookRecommend = new BaseUrl("/appConfigBookRecommend").addOther('updateTwo', '/updateTwo').addOther('getBookInfoList', '/getBookInfoList').addOther('getCategoryInfoList', '/getCategoryInfoList')

/**
 * app运营/推荐位管理/模块化分类书籍推荐
 */
export const appConfigCategoryBookRecommend = new BaseUrl("/appConfigCategoryBookRecommend")

/**
 * app运营/推荐位管理/卡片类型
 */
export const recommendCardType = new BaseUrl("/recommendCardType").addOther("cardTypeList", "/cardTypeList")
/**
 * app运营/推荐位管理/阅读器/独立页书籍推荐位
 */
 export const appConfigReadIndependent = new BaseUrl("/appConfigReadIndependent")
/**
 * app运营/推荐位管理/阅读器/广告解锁推荐书籍
 */
 export const advertRecommendBook = new BaseUrl('/advertRecommendBook').addOther('list', '/list').addOther('add', '/add').addOther('del', '/del').addOther('getRecommendBook', '/getRecommendBook').addOther('bookList', '/bookList').addOther('updateBook', '/updateBook')

 /**
 * app运营/产品配置/渠道管理配置
 */
export const channelManage = new BaseUrl('/bookChannel').addOther('page', '/page').addOther('add', '/add').addOther('getBookList', '/getBookList')

/** banner图 */
export const appConfigBannerList = new BaseUrl("/appConfigBannerList");

/** 新用户渠道跳转 */
export const appConfigNewUserStartJump = new BaseUrl("/appConfigNewUserStartJump");

/** 广告用户渠道跳转 */
export const appConfigADUserStartJump = new BaseUrl("/appConfigADUserStartJump");
/** 开关配置 */
export const appSwitchMember = new BaseUrl("/appSwitchMember")
.addOther("get", "/get").addOther("add", "/add").addOther("status", "/status").addOther("delete", "/delete");

//新增书币配置连续套餐类型
export const appConfigCoinVip = new BaseUrl("/appConfigCoinVip");
//阅读器盲盒弹窗配置
export const appConfigCoinRandom = new BaseUrl("/appConfigCoinRandom");
// /获取书币配置连续套餐类型列表
export const getCoinAndVipList = new BaseUrl("/appConfigCoinVip").addOther('getCoinAndVipList', '/getCoinAndVipList')
    // 新增书币配置连续套餐类型
export const saveCoinAndVip = new BaseUrl("/appConfigCoinVip").addOther('saveCoinAndVip', '/saveCoinAndVip')
    // 修改书币配置连续套餐类型
export const updateCoinAndVip = new BaseUrl("/appConfigCoinVip").addOther('updateCoinAndVip', '/updateCoinAndVip')
    // 投入产出
    //  /statkstkcal
export const statkstkcal = new BaseUrl("/statkstkcal")

/** APP运营/书城-精选/风云人物 */
export const catRecommend = new BaseUrl("/catRecommend").addOther("getBookInfoList", "/getBookInfoList").addOther("updateTwo", "/updateTwo")
export const catRecommendSort = new BaseUrl("/catRecommendSort").addOther("getList", "/getList")

/** 书籍分类 */
export const bookCategory = new BaseUrl("/bookcategory").addOther("getListByLeave", "/getListByLeave").addOther("selectByPreference", "/selectByPreference").addOther("selectBySiteAndParentIdChildren","/selectBySiteAndParentIdChildren")
    /** 书籍 */
export const AppBook = new BaseUrl("/book").addOther("getBookList", "/getBookList")
    .addOther("export","/export")
/** 书籍导入 */
export const bookImport = new BaseUrl("/bookImport").addOther("import", "/import")
    /** 书籍评分 */
export const appBookScore = new BaseUrl('/bookScore').addOther('getBookScoreByBookId', '/getBookScoreByBookId').addOther('update', '/update')

/** 插画 */
export const catInset = new BaseUrl("/catInset")
    .addOther("getListByRecommendId", "/getListByRecommendId")
    .addOther("listByParam", "/listByParam")
    .addOther("saveInset", "/saveInset")
    .addOther("updateInset", "/updateInset")
    .addOther("isTop", "/isTop")

/** 插画审核 */
export const catInsetCheck = new BaseUrl("/catInsetCheck")
    .addOther("listByParam", "/listByParam")
    .addOther("updateIsTop", "/updateIsTop")

/** 马甲 */
export const fakeUser = new BaseUrl("/appFakeUsers").addOther("addNameAll", "/addNameAll")

/** 热搜词 */
export const catHotBook = new BaseUrl("/catHotBook")

/** 顶部推荐书籍 */
// export const catBookRackTop = new BaseUrl("/catBookRackTop")

/** 书籍章节 */
export const appBookChapter = new BaseUrl("/appBookChapter")
    .addOther("auditList", "/auditList")
    .addOther("getInfoById", "/getInfoById")
    .addOther("updateById", "/updateById")

/** 书籍审核 */
export const appBookCheck = new BaseUrl("/appBookCheck")
    //获取书籍审核详情
    .addOther("getInfoById", "/getInfoById")
    //书籍审核通过或拒绝
    .addOther("updateBookCheck", "/updateBookCheck")

/** 书籍章节审核 */
export const appBookChapterCheck = new BaseUrl("/appBookChapterCheck")
    //获取章节审核详情
    .addOther("getInfoById", "/getInfoById")
    //章节审核通过或拒绝
    .addOther("updateBookChapterCheck", "/updateBookChapterCheck")
    //章节列表审核通过或拒绝
    .addOther('updateSelectBookChapterCheck', '/updateSelectBookChapterCheck')

/**头像库 */
export const appHeadImageLibrary = new BaseUrl("/appHeadImageLibrary").addOther("list", "/list").addOther("addImage", "/addImage")

/** 用户反馈 */
export const appUserFeedback = new BaseUrl("/appUserFeedback")
    /** 充值金额配置 */
export const catCoinRecharge = new BaseUrl("/catCoinRecharge")
    //阅读时长领书币
export const appConfigReadReward = new BaseUrl("/appConfigReadReward")

export const appVipPayConf = new BaseUrl("/appVipPayConf")
export const appConfigVipPremium = new BaseUrl("/appConfigVipPremium")
    .addOther('updateFlag', '/updateFlag')

/** 限时特惠配置 */ 
export const appConfigDiscount = new BaseUrl("/appConfigDiscount")
    // .addOther('updateState','/updateState')

/** 动态 */
export const catDynamic = new BaseUrl("/catDynamic")
    .addOther("isTop", "/isTop")

/** 新年flag */
export const activeFlag = new BaseUrl("/activeFlag")
export const activeFlagComment = new BaseUrl("/activeFlagComment")

/** 多语言文本配置 */
export const appConfigMultilingualTxt = new BaseUrl("/appConfigMultilingualTxt")

/** 默认语言文本配置 */
export const appConfigDefaultTxt = new BaseUrl("/AppConfigDefaultTxt").addOther("selectAppConfigDefaultTxt", "/selectAppConfigDefaultTxt").addOther("update", "/update").addOther("save", "/save")

/** 专题 */
export const appSubject = new BaseUrl("/appSubject")

/** 打赏配置 */
export const catGiverewardProp = new BaseUrl("/catGiverewardProp")

/** 提现配置 */
export const appConfigWithdrawal = new BaseUrl("/appConfigWithdrawal")

/** 签到奖励配置 */
export const appAwardContinueSign = new BaseUrl("/appAwardContinueSign")

/** Bonus兑换 */
export const appConfigExchangeList = new BaseUrl("/appConfigExchange")

/** 弹窗配置后台 */
export const appConfigPopWindow = new BaseUrl("/appConfigPopWindow").addOther("getPopWindowList", "/getPopWindowList").addOther("savePopWindow", "/savePopWindow").addOther("updatePopWindow", "/updatePopWindow")

/** 用户提现记录 */
export const catRmbExtract = new BaseUrl("/catRmbExtract").addOther("editState", "/editState").addOther("editStateAll", "/editStateAll")

/** 用户头像昵称审核 */
export const appUserUpdate = new BaseUrl("/appUserUpdate")

/** 评论 */
export const appComment = new BaseUrl("/appComment").addOther("editComments", "/editComments").addOther("editComment", "/editComment")

/** 危险词库 */
export const appDangerousLexicon = new BaseUrl("/appDangerousLexicon")

/** 作者审核 */
export const catAuthor = new BaseUrl("/catAuthor")

/** 插画评论 */
export const appInsetComment = new BaseUrl("/appInsetComment")

/** 热门标签 */
export const catHotTags = new BaseUrl("/catHotTags").addOther("getTags", "/getTags")

/** 渠道cp方 */
export const appChannelInfo = new BaseUrl("/appChannelInfo").addOther("getList", "/getList")

/** 评论模板 */
export const appCommentTemplate = new BaseUrl("/appCommentTemplate").addOther("deleteAll", "/deleteAll")

export const search = new BaseUrl("/search").addOther('list', '/list').addOther('toLead', '/toLead').addOther('toLead2', '/toLead2')


// /appConfigRedCent/save 一分钱充值
export const appConfigRedCent = new BaseUrl("/appConfigRedCent")
    //广告相关
    /**广告展示配置 */
export const adShowBook = new BaseUrl("/appConfigAdBook")
    .addOther("getBookInfoList", "/getBookInfoList")

/** 产品测试工具 */
export const tools = new BaseUrl("/tools")
    .addOther("getUserInfo", "/getUserInfo")
    .addOther("beansChange", "/beansChange")
    .addOther("changeUser", "/changeUser")
    .addOther("deleteUser", "/deleteUser")
    .addOther("clearUserDevice", "/clearUserDevice")
    //版本
    /**版本管理 */
export const versionManager = new BaseUrl("/version")
    .addOther("batchAdd", "/batchAdd")
export const channelPack = new BaseUrl("/channelPack")
    .addOther("channelPack", "/channelPack")
    .addOther("channelDownload", "/channelDownload")
    .addOther("getChannelListByAppId", "/getChannelListByAppId")
    //ios 版本控制
export const appVersionIos = new BaseUrl("/appVersionIos")


/** 版本选择*/
export const selectVersion = new BaseUrl("/selectVersion")
    .addOther("selectVersion", "")
    // /appConfigVipPremium/updateFlag 设置默认开关
export const updateFlag = new BaseUrl("/updateFlag")
    .addOther("appConfigVipPremium")
    /** 渠道包发布历史 */
export const appChannelPackCount = new BaseUrl("/appChannelPackCount")
    .addOther("getIdentifyCount", "/getIdentifyCount")
    .addOther("getAppChannelPackageById", "/getAppChannelPackageById")
    .addOther("publishChannelVersion", "/publishChannelVersion")
    /**项目管理 */
export const appProjectPackManage = new BaseUrl("/appProjectPackManage")
    .addOther("batchPack", "/batchPack")
    .addOther("batchChannelDownload", "/batchChannelDownload")

/**项目多渠道发布 */
export const appProjectpPackCount = new BaseUrl("/appProjectpPackCount")
    .addOther("getIdentifyCount", "/getIdentifyCount")
    .addOther("getAppProjectPackageById", "/getAppProjectPackageById")
    .addOther("batchPublishChannelVersion", "/batchPublishChannelVersion")

// 数据
/** 产品 */
export const productData = new BaseUrl("/productData")
    //查询DAU阅读情况  -- 分页+条件:根据type类型查询新老用户
    .addOther("getReadActionChannelListByType", "/getDauReadStatistics")
    .addOther("exportDauReadStatistics", "/exportDauReadStatistics")
    //查询每日各渠道新增情况 -- 分页+条件
    .addOther("getRealtimeAddList", "/getRealtimeAddList")
    // 查询实时新增数据（小时）
    .addOther("getNewUserByHourStatistics", "/getNewUserByHourStatistics")
    .addOther("exportNewUserByHourStatistics", "/exportNewUserByHourStatistics")
export const appStatisticsEverydayRouseUser = new BaseUrl("/appStatisticsEverydayRouseUser")

/** 搜索下载统计数据*/
export const appStatisticsToLeadBook = new BaseUrl("/appStatisticsToLeadBook")
    /** 新增阅读概况 */
export const newReadStatistics = new BaseUrl("/productData").addOther("newReadStatistics", "/getNewReadStatistics")
                                                            .addOther("exportNewReadStatistics", "/exportNewReadStatistics")
    /** 新增留存率 */
export const userNewAndRetained = new BaseUrl("/appStatisticsUserLogin").addOther("userNewAndRetained", "/getUserNewAndRetained")
                                                                        .addOther("exportUserNewAndRetained", "/exportUserNewAndRetained")

    /** 书籍数据统计 */
export const appStatisticsBook = new BaseUrl("/appStatisticsBookRead")
    //查询书籍阅读数据
    .addOther("getBookReadDataList", "/getBookReadDataList")
    //导出书籍阅读数据
    .addOther("exportToBookReadData", "/exportToBookReadData")
    //查询章节阅读数据
    .addOther("getBookChapterReadDataList", "/getBookChapterReadDataList")
    //导出章节阅读数据
    .addOther("exportToBookChapterReadData", "/exportToBookChapterReadData")
/** 导出记录 */
export const exportRecord = new BaseUrl("/exportRecord")
    .addOther("getExportRecordList","/getExportRecordList")
/** 书籍机翻 */
export const translation = new BaseUrl("/translation")
    .addOther("page","/page")
    .addOther("add","/add")
    .addOther("del","/del")
    .addOther("top","/")
    .addOther("again","/again")
    .addOther("export","/export")
    .addOther("exportFile","/exportFile")

/** 广告 */
export const adData = new BaseUrl("/adData")
    //查询广告位数据 按时
    .addOther("getAdHourList", "/getAdHourList")
    //查询广告位数据
    .addOther("getAdList", "/getAdList")
/** 广告解锁配置 */
export const appConfigAdUnlock = new BaseUrl("/appConfigAdUnlock");
    /**书籍 */
    //打赏额度统计
export const appStatisticsReward = new BaseUrl("/appStatisticsReward")
    //全本书籍评论统计
export const appStatisticsBookCommentNum = new BaseUrl("/appStatisticsBookCommentNum")
    //书籍章节评论统计
export const appStatisticsChapterCommentNum = new BaseUrl("/appStatisticsChapterCommentNum")

//书籍阅读人数统计
export const appStatisticsBookReadPeopleOfNum = new BaseUrl("/appStatisticsBookReadPeopleOfNum")
    //书籍收藏量统计
export const appStatisticsCollectNum = new BaseUrl("/appStatisticsCollectNum")
    //书籍首章阅读统计
export const appStatisticsFirstChapterRead = new BaseUrl("/appStatisticsFirstChapterRead")
    //书籍章节次日留存
export const appStatisticsChapterNextdayKeep = new BaseUrl("/appStatisticsChapterNextdayKeep")
    //书籍阅读完成数据
export const appStatisticsBookReadFinish = new BaseUrl("/appStatisticsBookReadFinish")
    //书籍订阅数据
export const appStatisticsBookSubscription = new BaseUrl("/appStatisticsBookSubscription")
                                            .addOther("appStatisticsBookSubscription", "/getBookSubscribe")
                                            .addOther("exportBookSubscribeData", "/exportBookSubscribeData")
    //章节订阅数据
export const appStatisticsChapterSubscription = new BaseUrl("/appStatisticsBookSubscription")
                                            .addOther("appStatisticsChapterSubscription", "/getBookChapterSubscribe")
                                            .addOther("exportBookChapterSubscribeData", "/exportBookChapterSubscribeData")

//书籍每日订阅数据
export const appStatisticsTodayBookSubs = new BaseUrl("/appStatisticsBookSubscription")
                                            .addOther("appStatisticsTodayBookSubs", "/getBookTodaySubscribe")
                                            .addOther("exportBookTodaySubscribeData", "/exportBookTodaySubscribeData")
//章节每日订阅数据
export const appStatisticsTodayChapterSubs = new BaseUrl("/appStatisticsBookSubscription")
                                            .addOther("appStatisticsTodayChapterSubs", "/getBookChapterTodaySubscribe")
                                            .addOther("exportBookChapterTodaySubscribeData", "/exportBookChapterTodaySubscribeData")

export const appStatisticsBookEveryday = new BaseUrl("/appStatisticsBookEveryday").addOther("preDay", "/preDay")

export const appStatisticsReadEveryday = new BaseUrl("/appStatisticsReadEveryday")
    //章节次日留存
export const appStatisticsChapterNextdayBackread = new BaseUrl("/appStatisticsChapterNextdayBackread")

/**
 * 活动数据
 * */
export const catStatisticsActiveFlag = new BaseUrl("/catStatisticsActiveFlag")

/**
 * 书籍数据搜索
 */
export const appBookDataSearch =
    new BaseUrl("/appBookDataSearch")
    .addOther("addSearch", "/addSearch")

/** 用户 */
//用户财务数据
export const appStatisticsUserFinance = new BaseUrl("/rechargeUserFinance").addOther("appStatisticsUserFinance", "/getPage")
                                        .addOther("exportUserSubscribeData", "/exportUserSubscribeData")
    //会员阅读数据
export const appStatisticsMemberRead = new BaseUrl("/appStatisticsMemberRead")
    //会员购买数据
export const appStatisticsMemberBuy = new BaseUrl("/memberBuy").addOther("statisticsAll","/statisticsAll").addOther("exportData","/exportData")
//阅读数据查询
export const readData = new BaseUrl("/readData").addOther("getReadDataByParams", "/getReadDataByParams")
                                        .addOther("exportToReadData", "/exportToReadData")
    /** 充值 */
    //交易订单明细
export const catPayOrder = new BaseUrl("/orderDetailed").addOther("statisticsAll", "/statisticsAll").addOther("exportData","/exportData").addOther("totalAmount","/totalAmount")
    /** 充值 */
    //每日喵币充值统计
export const appStatisticsRechargeCoinEveryday = new BaseUrl("/appStatisticsRechargeCoinEveryday")
    //每日会员购买统计
export const appStatisticsMemberBuyEveryday = new BaseUrl("/appStatisticsMemberBuyEveryday")
    //活动
export const userGetList = new BaseUrl("/userReturnCashCount").addOther("getList", "/getList")


//pc
export const pcConfigBanner = new BaseUrl('/pcConfigBanner').addOther('add', '/add').addOther('del', '/del')
    //查询咨询类型列表
export const pcAuthorAdvisoryReply = new BaseUrl('/pcAuthorAdvisoryReply')
    //查询咨询列表
export const pcAuthorAdvisory = new BaseUrl('/pcAuthorAdvisory')
export const reply = new BaseUrl('/pcAuthorAdvisoryReply').addOther('reply', '/reply')
    // 获取作者名称列表根据作者id
export const getAuthorNameByAuthorIds = new BaseUrl('/pcConfigMessagePush').addOther('getAuthorNameByAuthorIds', '/getAuthorNameByAuthorIds')
    // 查询消息推送模版列表
export const selectTemplate = new BaseUrl('/pcConfigMessageTemplate').addOther('selectTemplate', '/selectTemplate')
    // 查询作者列表
export const pcAuthor = new BaseUrl('/pcAuthor')
    // 查询签约书籍列表
export const authorWorkData = new BaseUrl('/authorWorkData').addOther('statisticsAll','/statisticsAll').addOther('updateData','/updateData')
    //查询咨询列表
export const selectAdvisoryTypeList = new BaseUrl('/pcAuthorAdvisory').addOther('selectAdvisoryTypeList', '/selectAdvisoryTypeList')

export const pcConfigNews = new BaseUrl('/pcConfigNews')
    // 查询推送信息列表
export const pcConfigMessagePush = new BaseUrl('/pcConfigMessagePush')
    //添加消息推送模版
export const pcConfigMessageTemplate = new BaseUrl('/pcConfigMessageTemplate')

// export const appConfigBookRecommends = new BaseUrl('/appConfigBookRecommend')

// 独家专区
export const getPcExclusiveZone = new BaseUrl('/pcExclusiveZone/list')
export const pcExclusiveZone = new BaseUrl('/pcExclusiveZone').addOther('bookList', '/bookList').addOther('updateBook', '/updateBook')

// 排行榜
export const pcRankingList = new BaseUrl('/pcRankingList').addOther('list', '/list').addOther('add', '/add').addOther('del', '/del').addOther('bookList', '/bookList').addOther('updateBook', '/updateBook')

// 分类
export const pcCategory = new BaseUrl('/pcCategory').addOther('del', '/del').addOther('add', '/add')

// 偏好
export const pcFavorite = new BaseUrl('/pcFavorite').addOther('list', '/list').addOther('bookList', '/bookList').addOther('updateBook', '/updateBook')

// 任务
export const appTaskList = new BaseUrl('/appTaskList').addOther('add', '/add').addOther('del', '/del')

// 任务库
export const appTaskLibrary = new BaseUrl('/appTaskLibrary').addOther('add', '/add').addOther('del', '/del')

// 九宫格抽奖配置
export const userLuckDraw = new BaseUrl('/luckDraw').addOther('list', '/list').addOther('update', '/update')

// 推送任务
export const pushTask = new BaseUrl('/pushTask').addOther('add', '/add').addOther('update', '/update').addOther('withdraw', '/withdraw')

// 新用户配置
export const newUserConfig = new BaseUrl('/newUserConfig').addOther('get', '/get').addOther('save', '/save')

// /orangecat/appConfigBookRecommend/pageAndParam