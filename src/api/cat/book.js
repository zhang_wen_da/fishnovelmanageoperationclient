import request from '@/router/axios';
import {catUrl,transUrl} from '@/config/env' 
import {
    Message
} from 'element-ui';

/**
 * 查询 table
 */
export const getList = (param) => request({
    url: catUrl + "/book/getBookList",
    method: "POST",
    data : {
        ...param
    }
})

/**
 * 修改书籍
 * @param {*} param
 */
export const updateBook = (param) => request({
    url: catUrl + "/book/updateBook",
    method: "POST",
    data : {
        ...param
    }
})

/**
 * 根据书籍id获取章节列表
 * @param {*} param
 */
export const getChapterListByBookId = (param) =>request({
    url: catUrl + "/book/getChapterListByBookIdAndParams",
    method: "POST",
    data : {
        ...param
    }
})
/**
 * 导入书籍
 * @param {*} param
 */
 export const getUploadTxt = (param) =>request({
    url: transUrl + "/translation/uploadTxt",
    method: "POST",
    data : {
        ...param
    }
})
/**
 * 新增章节
 * @param {*} param
 */
 export const getAdditionalChapter= (param) =>request({
    url: transUrl + "/translation/additionalChapter",
    method: "POST",
    data : {
        ...param
    }
})
/**
 * 移除书籍
 * @param {*} param
 */
 export const removeTxt = (param) =>request({
    url: transUrl + "/translation/",
    method: "DELETE",
    data : param
})
/**
 * 查询机翻队列
 */
 export const getMachineTurnover = (param) => request({
    url: transUrl + "/translation/list",
    method: "POST",
    data : {
        ...param
    }
})
/**
 * 查询机翻队列
 */
 export const getImportRecod = (param) => request({
    url: transUrl + "/translation/importRecod",
    method: "POST",
    data : {
        ...param
    }
})
/**
 * 添加书籍
 */
 export const translationAdd = (param) => request({
    url: transUrl + "/translation/",
    method: "POST",
    data : param
})
