// 配置编译环境和线上环境之间的切换
let baseUrl = '';
let iconfontVersion = ['567566_pwc3oottzol'];
let iconfontUrl = `//at.alicdn.com/t/font_$key.css`;
let codeUrl = `${baseUrl}/code`
const env = process.env

let catUrl = '';
let ddUrl = '';
let fishball = '';
let eggUrl = '';
let transUrl = '';

if (env.VUE_APP_CURRENTMODE == 'development') {
    //开发环境地址  线上
    catUrl = "http://47.243.190.252:7001/fishnovel"     //FishNovel 测试库
    transUrl = "http://8.210.81.53:8011"     //机翻测试
} else if (env.VUE_APP_CURRENTMODE == 'devtest') {
    //开发测试环境地址  本地
    catUrl = "http://localhost:7001/fishnovel"     //FishNovel 测试库
    transUrl = "http://localhost:8011"                                            //机翻测试
} else if (env.VUE_APP_CURRENTMODE == 'production') {
    //生产环境地址  线上
    catUrl = "http://8.210.230.253:7001/fishnovel"     //FishNovel 正式库
    transUrl = "http://8.210.81.53:8012"                                               //机翻正式
} else if (env.VUE_APP_CURRENTMODE == 'pre') {
    //生产环境地址  线上
    catUrl = "http://8.210.230.253:7001/fishnovel"     //FishNovel 正式库
    transUrl = "http://8.210.81.53:8012"
} else if (env.VUE_APP_CURRENTMODE == 'protest') {
    //生产测试环境地址  本地
    catUrl = "http://localhost:7001/fishnovel"     //FishNovel 正式库
    transUrl = "http://localhost:8012"
}
export {
    baseUrl,
    iconfontUrl,
    iconfontVersion,
    codeUrl,
    env,
    catUrl,
    ddUrl,
    fishball,
    eggUrl,
    transUrl
}