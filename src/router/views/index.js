import Layout from '@/page/index/'
export default [



    /**
     * 橘子猫start
     */

    //广告运营>广告配置>修改
    {
        path: '/org/ad',
        component: Layout,
        children: [{
            path: 'adidconfigedit',
            name: '',
            component: () =>
                import('@/views/orangecat/adoperation/adidconfigedit.vue'),
        },]
    },
    //广告运营>广告配置>添加
    {
        path: '/org/ad',
        component: Layout,
        children: [{
            path: 'adidconfigadd',
            name: '',
            component: () =>
                import('@/views/orangecat/adoperation/adidconfigadd.vue'),
        },]
    },
    //广告展示配置>广告配置>书籍列表
    {
        path: '/org/ad',
        component: Layout,
        children: [{
            path: 'bookinfo',
            name: '书籍详情',
            component: () =>
                import('@/views/orangecat/adoperation/adshow/adshowbooklist'),
        },]
    },

    //内容运营/书籍管理--章节列表
    {
        path: '/bookmanager',
        component: Layout,
        children: [{
            path: 'chapterlist',
            name: '章节详情',
            component: () =>
                import('@/views/orangecat/appoperation/content/booksmanagement/chapterlist'),
        },]
    },
    {
        path: '/featured',
        component: Layout,
        children: [{
            path: 'bookinfo',
            name: '书籍详情',
            component: () =>
                import('@/views/orangecat/appoperation/featured/bookinfo/bookinfo'),
        },]
    },
    {
        path: '/featured',
        component: Layout,
        children: [{
            path: 'categoryInfo',
            name: '分类详情',
            component: () =>
                import('@/views/orangecat/appoperation/featured/categoryInfo/categoryInfo'),
        },]
    },
    {
        path: '/featured',
        component: Layout,
        children: [{
            path: 'userinfo',
            name: '人物详情',
            component: () =>
                import('@/views/orangecat/appoperation/featured/userinfo/userinfo'),
        },]
    },
    //打包管理--渠道管理--下载渠道App
    {
        path: '/package',
        component: Layout,
        children: [{
            path: 'channeldownload',
            name: '下载渠道App',
            component: () =>
                import('@/views/orangecat/appversionmanager/channel/channeldownload'),
        }]
    },
    // 版本管理
    {
        path: '/orangecatversionpromotion',
        component: Layout,
        children: [{
            label: '渠道管理',
            path: '/channelmanager',
            component: () =>
                import('@/views/orangecat/appversionmanager/channelnmanager')
        },]
    },
    {
        path: '/project',
        component: Layout,
        children: [{
            name: '批量生成Apk包',
            path: 'channelbatchpack',
            component: () =>
                import('@/views/orangecat/appversionmanager/channel/channelbatchpack')
        },]
    },
    {
        path: '/projectpack',
        component: Layout,
        children: [{
            name: '批量下载项目app',
            path: 'projectdownload',
            component: () =>
                import('@/views/orangecat/appversionmanager/channel/projectdownload')
        },
        {
            name: '下载项目APP发布历史',
            path: 'projectAppbatchhistory',
            component: () =>
                import('@/views/orangecat/appversionmanager/channel/batchChannelPackageIdentify')
        },
        ]
    },
    {
        path: '/channelpackage',
        component: Layout,
        children: [{
            path: 'identifyhistory',
            name: 'App渠道包发布',
            component: () =>
                import('@/views/orangecat/appversionmanager/channel/channelPackageIdentify'),
        }]
    },
    //数据/书籍--每日阅读统计-章节详情&章节趋势图
    {
        path: '/everydayread',
        component: Layout,
        children: [{
            path: 'info',
            name: '章节详情',
            component: () =>
                import('@/views/orangecat/data/book/everydayread/chapter'),
            }
        ]
    },
    //数据/书籍--付费用户每日阅读统计-章节详情&章节趋势图
    {
        path: '/freeeverydayread',
        component: Layout,
        children: [{
            path: 'info',
            name: '章节详情',
            component: () =>
                import('@/views/orangecat/data/book/freeeverydayread/chapter'),
            }
        ]
    },
    //数据/书籍--新用户每日阅读统计-章节详情
    {
        path: '/newuserread',
        component: Layout,
        children: [{
            path: 'info',
            name: '章节详情',
            component: () =>
                import('@/views/orangecat/data/book/newuserread/chapter'),
            }
        ]
    },
    //数据/书籍--累计阅读统计-章节详情
    {
        path: '/totalread',
        component: Layout,
        children: [
            {
                path: 'info',
                name: '章节统计',
                component: () =>
                    import('@/views/orangecat/data/book/totalread/chapter'),
            },
        ]
    },
    //数据/书籍--书籍订阅-章节订阅
    {
        path: '/chaptersubscription',
        component: Layout,
        children: [{
            path: 'chaptersubscription',
            name: '章节订阅',
            //src\views\orangecat\data\book\new\chaptersubscription\chaptersubscription.vue
            component: () =>
                import('@/views/orangecat/data/book/new/chaptersubscription/chaptersubscription'),
        }]
    },
    //数据/书籍--书籍每日订阅-章节订阅
    {
        path: '/chaptersubs',
        component: Layout,
        children: [{
            path: 'chaptersubscription',
            name: '章节订阅',
            component: () =>
                import('@/views/orangecat/data/book/everydaysubs/chaptersubs/chapter'),
        }]
    },
    //推荐位管理-书架-顶部书籍推荐  编写自定义表单--添加&编辑
    {
        path: '/topbookrecommendation',
        component: Layout,
        children: [{
            path: 'edit',
            name: '顶部书籍编辑',
            component: () =>
                import('@/views/orangecat/appoperation/featured/bookrack/topbook/topbookedit'),
        },
        {
            path: 'add',
            name: '顶部书籍添加',
            component: () =>
                import('@/views/orangecat/appoperation/featured/bookrack/topbook/topbookadd'),
        }
        ]
    },
       //推荐位管理-书架-顶部书籍推荐  编写自定义表单--添加&编辑
    {
        path: '/featuredFirst',
        component: Layout,
        children: [{
            path: 'advertBookinfo',
            name: '书籍详情',
            component: () =>
                import('@/views/orangecat/appoperation/featured/reader/advertBookinfo'),
        },]
    },
       //产品配置管理-渠道配置  编写自定义表单--添加&编辑
    {
        path: '/featuredFirst',
        component: Layout,
        children: [{
            path: 'channelManageBookinfo',
            name: '书籍详情',
            component: () =>
                import('@/views/orangecat/appoperation/productconfig/channelManageBookinfo'),
        },]
    },
    //作品审核  编写自定义表单--作品详情
    {
        path: '/works',
        component: Layout,
        children: [{
            path: 'worksinfo',
            name: '作品查看',
            component: () =>
                import('@/views/orangecat/auditor/contentaudit/works/worksinfo'),
        }]
    },
    //作品审核  编写自定义表单--作品章节详情
    {
        path: '/works',
        component: Layout,
        children: [{
            path: 'worksinfochapter',
            name: '作品查看',
            component: () =>
                import('@/views/orangecat/auditor/contentaudit/works/worksinfochapter'),
        }]
    },
    //插画管理  编写自定义表单--添加&编辑
    {
        path: '/illustration',
        component: Layout,
        children: [{
            path: 'edit',
            name: '插画编辑',
            component: () =>
                import('@/views/orangecat/appoperation/content/illustration/illustrationedit'),
        },
        {
            path: 'add',
            name: '插画新增',
            component: () =>
                import('@/views/orangecat/appoperation/content/illustration/illustrationadd'),
        },
        ]
    },
    //动态管理  编写自定义表单--添加&编辑
    {
        path: '/dynamic',
        component: Layout,
        children: [{
            path: 'edit',
            name: '动态编辑',
            component: () =>
                import('@/views/orangecat/appoperation/content/dynamic/dynamicedit'),
        },
        {
            path: 'add',
            name: '动态新增',
            component: () =>
                import('@/views/orangecat/appoperation/content/dynamic/dynamicadd'),
        },
        ]
    },
    //动态管理  编写自定义表单--添加&编辑
    {
        path: '/commenttemplate',
        component: Layout,
        children: [{
            path: 'edit',
            name: '评论编辑',
            component: () =>
                import('@/views/orangecat/appoperation/content/commenttemplate/commenttemplateedit'),
        },
        {
            path: 'add',
            name: '评论新增',
            component: () =>
                import('@/views/orangecat/appoperation/content/commenttemplate/commenttemplateadd'),
        },
        ]
    },
    //马甲管理  编写自定义表单--添加&编辑
    {
        path: '/fakeuser',
        component: Layout,
        children: [{
            path: 'edit',
            name: '马甲编辑',
            component: () =>
                import('@/views/orangecat/appoperation/content/fakeuser/fakeuseredit'),
        },
        {
            path: 'add',
            name: '马甲新增',
            component: () =>
                import('@/views/orangecat/appoperation/content/fakeuser/fakeuseradd'),
        },
        {
            path: 'addall',
            name: '马甲批量新增',
            component: () =>
                import('@/views/orangecat/appoperation/content/fakeuser/fakeuseraddall'),
        },
        {
            path: 'headimage',
            name: '马甲头像库',
            component: () =>
                import('@/views/orangecat/appoperation/content/fakeuser/fakeheadimage'),
        },
        ]
    },
    {
        path: '/wel',
        component: Layout,
        redirect: '/wel/index',
        children: [{
            path: 'index',
            name: '首页',
            // meta: {
            //   i18n: 'dashboard'
            // },
            component: () =>
                import( /* webpackChunkName: "views" */ '@/views/wel/index')
        }, {
            path: 'dashboard',
            name: '控制台',
            meta: {
                i18n: 'dashboard',
                menu: false,
            },
            component: () =>
                import( /* webpackChunkName: "views" */ '@/views/wel/dashboard')
        }]
    }, {
        path: '/form-detail',
        component: Layout,
        children: [{
            path: 'index',
            name: '详情页',
            meta: {
                i18n: 'detail'
            },
            component: () =>
                import( /* webpackChunkName: "views" */ '@/views/util/form-detail')
        }]
    }, {
        path: '/info',
        component: Layout,
        redirect: '/info/index',
        children: [{
            path: 'index',
            name: '个人信息',
            meta: {
                i18n: 'info'
            },
            component: () =>
                import( /* webpackChunkName: "views" */ '@/views/user/info')
        }, {
            path: 'setting',
            name: '个人设置',
            meta: {
                i18n: 'setting'
            },
            component: () =>
                import( /* webpackChunkName: "views" */ '@/views/user/setting')
        }]
    },
    /** PC站 */
    {
        path: '/featuredFirst',
        component: Layout,
        children: [{
            path: 'bookinfo',
            name: '书籍详情',
            component: () =>
                import('@/views/orangecat/pc/featuredFirst/home/bookinfo'),
        },]
    },
    {
        path: '/featuredFirst',
        component: Layout,
        children: [{
            path: 'rankingsBookinfo',
            name: '书籍详情',
            component: () =>
                import('@/views/orangecat/pc/featuredFirst/home/rankingsBookinfo'),
        },]
    },
    {
        path: '/featuredFirst',
        component: Layout,
        children: [{
            path: 'favoriteBookinfo',
            name: '书籍详情',
            component: () =>
                import('@/views/orangecat/pc/featuredFirst/home/favoriteBookinfo'),
        },]
    },
]